/*

To Do:

Let admins check on the status of borrowed items
Let admins whitelist through PM'd commands
Check kmails to see who has returned what


*/

import <deps/clanfortune.ash>;
import <deps/Unlock_Dread.ash>;
import <util/helperFunctions.ash>;

void main(string sender, string message, string channel){

	// *** Strip the message of any chat effects.
	print("Starting the strip dance.");
    string stripped_message = strip_chat_messages(message);

	// *** Set up the data structure that will store the day's clan chat.
	string [int] clan_chat;
	file_to_map("ClanChat.txt", clan_chat);
	int next_chat_key = count(clan_chat);

	// *** Set up the data structure to store who borrows what.
	string [string] borrow_log;
	file_to_map("BorrowLog.txt", borrow_log);

	// *** Currently borrowable stock.
	int [string] borrow_stock;
	file_to_map("borrow_stock.txt", borrow_stock);

	if (channel == "Events") {
		if (contains_text(message, "You have been invited to consult Madame Zatara")) {
			clanfortune_parse("defaults,1st response,beer");
			clanfortune_parse("defaults,2nd response,robin");
			clanfortune_parse("defaults,3rd response,thin");
			clanfortune_parse("respond all");
		}
	}
	else if (channel == "/clan") {

		if (stripped_message == "!chatlog") {
			chatlog(sender, clan_chat, next_chat_key);
		}
		else if (responses contains stripped_message) {
			respond(stripped_message, sender);
		}
		else {
			string hour = to_string(gametime_to_int() / (86400000 / 24));
			string minute = to_string((gametime_to_int() / (86400000 / 1440)) % 60);
			clan_chat[next_chat_key] = "[" + hour + ":" + minute + "] " + sender + ": " + stripped_message;
			map_to_file(clan_chat,"ClanChat.txt");
		}
	}

	else if (channel == "") {

		if (length(stripped_message) > 7 && substring(stripped_message, 0, 7) == "borrow " && whitelisted(sender)) {
			if ( lend(sender, substring(stripped_message, 7)) ) {
				borrow_log[today_to_string() + " at " + time_to_string()] = sender + ", " + substring(stripped_message, 7);
			}
			else {
				// *** Failures taken care of in the lend function.
			}
		}
		else if (length(stripped_message) > 8 && substring(stripped_message, 0, 8) == "jackass ") {
			// use_skill(1, $skill[Jackasses' Symphony of Destruction], substring(stripped_message, 8));
		}
		else if (length(stripped_message) > 8 && substring(stripped_message, 0, 8) == "clanhop ") {
			if (whitelisted(sender)) {
				clanhop(sender, substring(stripped_message, 8));
			}
			else {
				chat_private(sender, "You don't have permission to use the clanhop command.");
			}
		}
		else if (stripped_message == "cagebait" && admin(sender)) {
			print("Cagebait message recieved and user is an admin!");
			cagebait(sender);
    	}
		else if (stripped_message == "!chatlog") {
			chatlog(sender, clan_chat, next_chat_key);
		}
		else if (stripped_message == "adv" || stripped_message == "adventures") {
			chat_private(sender, "I have " + my_adventures() + " adventures remaining.");
		}
		else if (stripped_message == "gnaw" && admin(sender)) {
			run_choice(1);
		}
		else if (stripped_message == "!help") {
			chat_private(sender, "Hello!\n\nIn the clan chat:\n\n!chatlog\n\nYou might also try poking d0rfl, as well as some other things :)\n\nIn a private message:\n\n!chatlog\n!ghostly\n!tenacity\n!empathy\n!spiky\n!reptilian\n!astral\n!elemental\n!jalapeno\n!antibiotic\n!scary\n!moxious\n!magical\n!cletus\n!ballad\n!polka\n!destruction\n!leon\n!brawnee\n!psalm\n!shanty\n!aloysius\n!ode\n!sonata\n!inigo\n!richie\n!chorale\n!bulls\nthrills\n!hells");
		}
		else {
			switch(stripped_message) {
				case "!ghostly":
					use_skill(1, $skill[Ghostly Shell], sender);
					break;
				case "!tenacity":
					use_skill(1, $skill[Tenacity of the Snapper], sender);
					break;
				case "!empathy":
					use_skill(1, $skill[Empathy of the Newt], sender);
					break;
				case "!spiky":	
					use_skill(1, $skill[Spiky Shell], sender);
					break;
				case "!reptilian":	
					use_skill(1, $skill[Reptilian Fortitude], sender);
					break;
				case "!astral":
					use_skill(1, $skill[Astral Shell], sender);
					break;
				case "!elemental":
					use_skill(1, $skill[Elemental Saucesphere], sender);
					break;
				case "!jalapeno":
					use_skill(1, $skill[Jalape&ntilde;o Saucesphere], sender);
					break;
				case "!antibiotic":
					use_skill(1, $skill[Antibiotic Saucesphere], sender);
					break;
				case "!scary" :
					use_skill(1, $skill[Scarysauce], sender);
					break;
				case "!moxious":
					use_skill(1, $skill[The Moxious Madrigal], sender);
					break;
				case "!magical":
					use_skill(1, $skill[The Magical Mojomuscular Melody], sender);
					break;
				case "!cletus":
					use_skill(1, $skill[Cletus's Canticle of Celerity], sender); break;
				case "!ballad":
				use_skill(1, $skill[The Power Ballad of the Arrowsmith], sender); break;
				case "!polka":
				use_skill(1, $skill[The Polka of Plenty], sender); break;
				case "!destruction":
				use_skill(1, $skill[Jackasses' Symphony of Destruction], sender); break;
				case "!leon":
				use_skill(1, $skill[Fat Leon's Phat Loot Lyric], sender); break;
				case "!brawnee":
				use_skill(1, $skill[Brawnee's Anthem of Absorption], sender); break;
				case "!psalm":
				use_skill(1, $skill[The Psalm of Pointiness], sender); break;
				case "!shanty": use_skill(1, $skill[Stevedave's Shanty of Superiority], sender); break;
				case "!aloysius": use_skill(1, $skill[Aloysius' Antiphon of Aptitude], sender); break;
				case "!ode": use_skill(1, $skill[The Ode to Booze], sender); break;
				case "!sonata": use_skill(1, $skill[The Sonata of Sneakiness], sender); break;
				case "!inigo":
					string inigoCasts = get_property("_inigosCasts");
					if(inigoCasts != "5") {
						use_skill(1, $skill[Inigo's Incantation of Inspiration], sender);
						break;
					}
					else {
						chat_private(sender, "That didn't work, I probably don't have any casts left :(");
						break;
					}
				case "!richie":
					string richieCasts = get_property("_thingfinderCasts");
					if(richieCasts != "10") {
						use_skill(1, $skill[The Ballad of Richie Thingfinder], sender);
						break;
					}
					else {
						chat_private(sender, "That didn't work, I probably don't have any casts left :(");
						break;
					}
				case "!chorale":
					string choraleCasts = get_property("_companionshipCasts");
					if(choraleCasts != "10") {
						use_skill(1, $skill[Chorale of Companionship], sender);
						break;
					}
					else {
						chat_private(sender, "That didn't work, I probably don't have any casts left :(");
						break;
					}
				case "!bulls": use_skill(1, $skill[Carol of the Bulls], sender); break;
				case "!hells": use_skill(1, $skill[Carol of the Hells], sender); break;
				case "!thrills": use_skill(1, $skill[Carol of the Thrills], sender); break;
			}
		}
		map_to_file(borrow_log,"BorrowLog.txt");
	}
}