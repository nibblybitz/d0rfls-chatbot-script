import <util/helperData.ash>;

// *************************
// *** Available helper data
// *************************
// *** responses
// *** patterns_to_strip
// *** whitelist

string strip_chat_messages(string line) {

    string message_to_strip = line;

    matcher stripper = create_matcher("<.+?>", message_to_strip);
    message_to_strip = replace_all(stripper, "");

    stripper = create_matcher(" -hic-", message_to_strip);
    message_to_strip = replace_all(stripper, "");

    stripper = create_matcher(";", message_to_strip);
    message_to_strip = replace_all(stripper, "");

    return message_to_strip;
}

void chatlog(string sender, string [int] cc, int key) {

    buffer kmail;

    // *** If there are fewer than 30 messages, just send everything.

    if (key < 30) {
        foreach x in cc {
            append(kmail, cc[x] + "\n");
        }
    }

    // *** Else there are 30 or more, only send last 30.

    else {
        for x from (key - 30) to (key - 1) {
            append(kmail, cc[x] + "\n");
        }
    }

    cli_execute("kmail to " + sender + " || " + kmail);
}

void cagebait(string sender) {
    // *** Makes certain that mafia doesn't gnaw through the bars or pick fights.
    // *** This is likely unneccessary because the script uses visit_url(), however, it's a good failsafe.
    set_property("choiceAdventure197", 3);
    set_property("choiceAdventure198", 3);
    set_property("choiceAdventure199", 3);
    set_property("choiceAdventure211", 0);
    set_property("choiceAdventure212", 0);

    set_property("battleAction","custom combat script");
    set_property("customCombatScript","Cagebait.ccs");

    if (visit_url("clan_hobopolis.php").contains_text("The Old Sewers")) {
        chat_private(sender, "Entering the sewers now.");
        // *** This script doesn't handle combat, so we need to make certain we only encounter noncombats.
        buy( 1, $item[stench jelly] );
        chew( 1, $item[stench jelly] );

        boolean done = false;
        
        while (!done) {
            string page = visit_url("adventure.php?snarfblat=166");

            // *** If we hit the cage, notify and stop looping.
            if (page.contains_text("walking down one of the myriad sewer tunnels when you sense danger")) {
                chat_private(sender, "I'm caught in the cage!");
                done = true;
                repeat {refresh_status();}
                until (true);
            }
            // *** Open grates
            else if (page.contains_text("The right branch leads into a much deeper")) {
                run_choice(3);
            }
            // *** Choose the combat, CLEESH and destroy.
            else {
                run_choice(2);
            }
        }
    }
    else {
        chat_private(sender, "I don't appear to have access to the sewers.");
    }
}

boolean whitelisted(string sender) {
    return (whitelist contains to_lower_case(sender));
}

void send_whitelist(string sender) {
    
}

boolean admin(string sender) {
    if (whitelisted(sender)) {
        if (whitelist[to_lower_case(sender)] == "admin") {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

void clanhop(string sender, string clan) {
    string current_clan = get_clan_name();
    cli_execute("clanhop " + clan);
    if (current_clan == get_clan_name()) {
        chat_private(sender, "Stayed in " + current_clan);
    }
    else {
        chat_private(sender, "Transferred to " + get_clan_name());
    }
}

boolean lend(string sender, string thing) {
    if (borrowable contains to_lower_case(thing)) {
        if (item_amount(to_item(thing)) > 0) {
            cli_execute("send " + thing + " to " + sender + " || Here you go!");
            return true;
        }
        else {
            chat_private(sender, "I don't currently have one of those for you to borrow.");
        }
    }
    else {
        chat_private(sender, "Either I don't lend that item out, or you didn't spell your request correctly.");
    }
    return false;
}

void respond(string message, string sender) {

    int rando = random(100);
    buffer response;

    if (rando < 60) {
        response.append(responses[message]["common"][random(count(responses[message]["common"]))]);
    }
    else if (rando > 60 && rando < 90) {
        response.append(responses[message]["uncommon"][random(count(responses[message]["uncommon"]))]);
    }
    else if (rando > 89) {
        response.append(responses[message]["rare"][random(count(responses[message]["rare"]))]);
    }

    response.replace_string("$$$", sender);
    chat_clan(response);
}