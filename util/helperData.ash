// ************************************
// *** These need to be customized. ***
// ************************************

string [string] whitelist;

whitelist["tobz"] = "admin";
whitelist["aeshma"] = "admin";
whitelist["the inexplicable blue"] = "admin";
whitelist["ashgm"] = "admin";
whitelist["thoth19"] = "admin";
whitelist["ugotponed12"] = "admin";
whitelist["will408914"] = "admin";
whitelist["covfefe"] = "admin";
whitelist["iloath"] = "admin";
whitelist["trueturtle"] = "clannie";
whitelist["big oli"] = "clannie";
whitelist["shootgun47"] = "clannie";
whitelist["kabiffany"] = "clannie";

int [string] borrowable;

borrowable["haiku katana"] = 1;
borrowable["mafia pointer finger ring"] = 1;
borrowable["browser cookie"] = 1;
borrowable["spectral pickle"] = 1;

string [string][string][int] responses;

responses["hits d0rfl"]["common"][0] = "\/me arrests " + "$$$" + " for violent roleplay";
responses["hits d0rfl"]["common"][1] = "Citizen " + "$$$" + ", Cease This Foolishness";
responses["hits d0rfl"]["uncommon"][0] = "https://media.giphy.com/media/xT1XGLzxTFgIwxcbcY/giphy.gif";
responses["hits d0rfl"]["uncommon"][1] = "https://media.giphy.com/media/8Eup943BRqxbO/giphy.gif";
responses["hits d0rfl"]["uncommon"][2] = "https://media.giphy.com/media/3oGNDyPEvASWx6onni/giphy.gif";
responses["hits d0rfl"]["rare"][0] = "\/me envelopes the puny " + "$$$" + " in a crushing bearhug of justice";
responses["hits d0rfl"]["rare"][1] = "\/me prepares to go all Iron Giant on " + "$$$" + "'s $%&";

responses["pokes d0rfl"]["common"][0] = "\/me chuckles gently as " + "$$$" + " winces in pain";
responses["pokes d0rfl"]["common"][1] = "\/me humors " + "$$$" + ", pretends to be a little startled";
responses["pokes d0rfl"]["common"][1] = "\/me expresses mild shock";
responses["pokes d0rfl"]["uncommon"][0] = "https://media.giphy.com/media/3x5nIjlszTBQs/giphy.gif";
responses["pokes d0rfl"]["uncommon"][1] = "https://media.giphy.com/media/jCENc3aA4fLJm/giphy.gif";
responses["pokes d0rfl"]["uncommon"][2] = "https://media.giphy.com/media/3ohfFH3gJpepwS5DEY/giphy.gif";
responses["pokes d0rfl"]["uncommon"][3] = "https://media.giphy.com/media/aZSMD7CpgU4Za/giphy.gif";
responses["pokes d0rfl"]["rare"][0] = "\/me calls Meerk Zookenflorb to complain";
responses["pokes d0rfl"]["rare"][1] = "\/me wonders why you've squished your strange, noodley appendage up against his beautiful baked clay";

responses["pats d0rfl"]["common"][0] = "\/me stoically accepts " + "$$$" + "'s affection";
responses["pats d0rfl"]["common"][1] = "\/me allows the creature " + "$$$" + " to continue its strange physical\/emotional ritual";
responses["pats d0rfl"]["uncommon"][0] = "https://media.giphy.com/media/nfKdTuhshHG7K/giphy.gif";
responses["pats d0rfl"]["uncommon"][1] = "https://media.giphy.com/media/N0CIxcyPLputW/giphy.gif";
responses["pats d0rfl"]["uncommon"][2] = "https://media.giphy.com/media/ypZgIwd1T71Uk/giphy.gif";
responses["pats d0rfl"]["uncommon"][3] = "https://media.giphy.com/media/qicEpin9kTVJe/giphy.gif";
responses["pats d0rfl"]["rare"][0] = "\/me considers reciprocating by patting dat booty, but thinks that might send the wrong message";
responses["pats d0rfl"]["rare"][1] = "\/me wonders why you've squished your strange, noodley, al dente appendage up against his beautiful baked clay";
responses["pats d0rfl"]["rare"][2] = "Who Touched Me? Somebody Hath Touched Me.";

responses["d0rfl, what's the meaning of life?"]["common"][0] = "42.";
responses["d0rfl, what's the meaning of life?"]["common"][1] = "I Have Not Been Programmed To Care About Your Problems.";
responses["d0rfl, what's the meaning of life?"]["uncommon"][0] = "https://media.giphy.com/media/NxEsKrLJXPuwg/giphy.gif";
responses["d0rfl, what's the meaning of life?"]["uncommon"][1] = "https://media.giphy.com/media/y65VoOlimZaus/giphy.gif";
responses["d0rfl, what's the meaning of life?"]["uncommon"][2] = "https://media.giphy.com/media/Lg7By5kaXegSI/giphy.gif";
responses["d0rfl, what's the meaning of life?"]["rare"][0] = "Life Has No Meaning A Priori. It Is Up To You To Give It A Meaning, And Value Is Nothing But The Meaning That You Choose.";
responses["d0rfl, what's the meaning of life?"]["rare"][1] = "The Meaning Of Life Is Getting Your !@#$ Together, " + "$$$" + ".";
