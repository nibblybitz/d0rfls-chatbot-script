// a not-so-simple script for clanhopping
// clanhop v1.2

script "clanhop.ash";
notify <Bale>;

string [int, int] get_whitelist() {
	string url = visit_url("clan_signup.php");
	int start = index_of(url, "Whitelist List");
	int end = index_of(url, "You are currently whitelisted by");
	string clans_raw = substring(url, start, end);
	print(substring(url, end, end + 41), "#002080");
	return  group_string(clans_raw, "option value=([0-9]+)>([^<]+)");
	// if you have XX WLs, the fullstop gets chopped off
	// if you have XXX WLs, the fullstop AND s get chopped off, sue me, I'm lazy
}

void print_whitelist(string [int, int] clandata) {
	print("You are a member of these clans", "#002080");
	foreach i in clandata
		#print(clandata[i,1]);
		print("... "+ clandata[i,2]);
}

boolean clanhop(string targetclan) {
	targetclan = targetclan.to_lower_case();
	string [int, int] clandata = get_whitelist();
	int target_id;
	string target_name, clan_name;
	boolean found = false;
	boolean text_found = false;
	int clans = 0;
	
	void match_clan(string clan_name, int clanid) {
		print("Matching clan found: " + clan_name);
		if(clans > 0) {
			if(length(clan_name) < length(target_name)) {
				target_id = clanid;
				target_name = clan_name;
			}
		} else {
			target_id = clanid;
			target_name = clan_name;
			found = true;
		}
		clans = clans + 1;
	}
	
	string match_string = "^(the |)"; 	// This is used to match initials like "HCN for Hardcore Nation"
	for i from 0 to length(targetclan) -1 {
		match_string = match_string + substring(targetclan, i, i+1)+ ".+";
	}
	
	foreach i in clandata {
		int clanid = to_int(clandata[i,1]);
		clan_name = clandata[i,2];
		if(contains_text(clan_name.to_lower_case(), targetclan)) {
			print("Matching clan found: " + clan_name);
			if(clans > 0) {
				if(length(clan_name) < length(target_name) || !text_found) {
					target_id = clanid;
					target_name = clan_name;
				}
			} else {
				target_id = clanid;
				target_name = clan_name;
				found = true;
			}
			text_found = true;
			clans = clans + 1;
		} else if(!text_found) { 	// Test for clan initials only if a text match fails.
			matcher initials = create_matcher(match_string, clan_name.to_lower_case());
			if(initials.find()) {
				print("Matching clan found: " + clan_name);
				if(clans > 0) {
					if(length(clan_name) < length(target_name)) {
						target_id = clanid;
						target_name = clan_name;
					}
				} else {
					target_id = clanid;
					target_name = clan_name;
					found = true;
				}
				clans = clans + 1;
			}
		}
	}
	if(clans > 1) {
		print("Multiple matches found! Making a guess as to the most precise match.", "red");
		print("Attempting hop to clan: " + target_name, "#002080");
	}
	if(found) {
		string url = visit_url("showclan.php?recruiter=1&whichclan="+ target_id +"&pwd&whichclan=" + target_id + "&action=joinclan&apply=Apply+to+this+Clan&confirm=on");
		// Check that we made it?
		if(contains_text(url, "clanhalltop.gif")) {
			print("Welcome to your new clan, "+target_name+ "!", "green");
			return true;
		} else if(contains_text(url, "You can't apply to a clan you're already in."))
			print("You're already a member of the clan, "+target_name+ ".", "olive");
		else {
			print("Something may have gone wrong.", "red");
			print_whitelist(clandata);
		}
	} else {
		print("Clan not found", "red");
		print_whitelist(clandata);
	}
	return false;
}

void main(string targetclan) {
	clanhop(targetclan);
}
