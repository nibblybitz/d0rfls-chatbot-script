notify pazsox;
/*
v2.0 The attic needs to be unlocked before you can banish sleaze/grab music parts
v1.9 Fixed small bug where scipt would see Village, and not recognize element to be banished
v1.8 Added monster banish, and fixed issue with checking locks, and aborting if an issue
v1.7 Added ability to unlock each zone individually, and restart.
v1.6 Added banish for each zone/element.  Script knows if element has been banished.
v1.5 Added help for setting up Dread, including unlocking areas
v1.4 Made script stop if skeleton key can not be obtained, rather than have it continue to try, and fail, and some other minor changes again.
v1.3 Cleaned up the code a little, changed some messages, and other minor changes (thanks to bale's suggestion)
v1.2 Re-Arranged script, so it checks if an areas is unlocked BEFORE it tries to buy a key, and other stuff (much better this way)
v1.1 Added check to see if areas have been unlocked
v1.0 release: If an area is already unlocked, the script will just try to unlock it anyways.  I have to put in a check for that.
*/
		print_html ("<font color=008000>Here's the commands I understand, anything in <font color=7900a7><b>BOLD:"); 
		print_html ("To unlock the Cabin, Tallest Tree, Village, Old Dukes Estate, Great Hall, and Tower: <font color=7900a7><b>All");
		print_html ("Unlock Zones: <font color=7900a7><b>Cabin</b></font>, or Tallest <font color=7900a7><b>Tree</b></font>, or<font color=7900a7><b> Village</b></font>, or Old Dukes <font color=7900a7><b>Estate</b></font>, or Great <font color=7900a7><b>Hall</b></font>, or the<font color=7900a7><b> Tower</b></font> only");
		print_html ("Banish: <font color=7900a7><b>Wolves</b></font>,<font color=7900a7><b> Skeletons</b></font>,<font color=7900a7><b> Zombies</b></font>,<font color=7900a7><b> Bugbears</b></font>,<font color=7900a7><b> Vampires</b></font>, or<font color=7900a7><b> Ghosts</b></font>");
		print_html ("To banish an Element: ZoneElement");
		print_html ("Zones: <font color=7900a7><b>Village</font></b>, <font color=7900a7><b>Forest</font></b>, <font color=7900a7><b>Castle</font></b> Elements: <font color=7900a7><b>Hot</font></b>, <font color=7900a7><b>Cold</font></b>, <font color=7900a7><b>Spooky</font></b>, <font color=7900a7><b>Sleazy</font></b>, <font color=7900a7><b>Stinky");
		print_html ("Example Element banish: <font color=7900a7>Foresthot</font>, or <font color=7900a7>Castlestinky</font>, or <font color=7900a7>Villagespooky</font>, any combo works.");
		print_html ("<font color=#ff0000>NOTE:</font> The Script will continue to reload until you decide to cancel.");
		print(" ");
		print("Below this line is a basic Dreadsylvania help guide");
		print_html ("Basic info on <font color=#0000cc>Kisses</font>: <font color=7900a7><b>Kisses");
		print_html ("Basic info on <font color=#0000cc>Hard Mode Bosses: <font color=7900a7><b>Boss");
		print_html ("Best items Monsters Drop: <font color=7900a7><b>Drops");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Great Wolf of the Air</font>:<font color=7900a7><b> Wolf");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Falls-From-Sky</font>:<font color=7900a7><b> Falls");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Mayor Ghost</font>:<font color=7900a7><b> Mayor");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Zombie Homeowners' Association</font>:<font color=7900a7><b> ZHA");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Count Drunkula</font>:<font color=7900a7><b> Drunkula");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>The Unkillable Skeleton<font color=7900a7><b> Skeleton");
		print_html ("To Exit this script, click Cancel button");

void wolves()
//spooky cabin werewolves
 {
	print("Checking out the lock on the Cabin first", "Green");

			string log = visit_url( "clan_raidlogs.php" );
		{
			if(log.contains_text( ") unlocked the attic of the cabin" ))
			{	print( "The attic of the cabin has been unlocked.", "green" );	}
			else
			{	print("The attic of the cabin has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}

	print("Checking out the lock on the Master Suite next", "Green");
		
			if(log.contains_text( ") unlocked the master suite" ))
			{	print( "The master suiten has been unlocked, initiating banish", "green" );	}
			else
			{	print("The master suite has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}
		{	string [int] drove;
	foreach place in $strings[some werewolves out of the forest] 
	 {
			drove = split_string(log, place);
			if(count(drove) == 3)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " times.  No banishing needed in this zone", "green");
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash");
			return;	}
			else if(count(drove) == 2)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " time.  Continuing with banish");	}
			else { print("No one drove " + place); }
	}}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1");
	visit_url("choice.php?pwd&whichchoice=721&option=3"); //attic
	visit_url("choice.php?pwd&whichchoice=724&option=2"); //banish werewolves

//duke's estate
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=6");
	visit_url("choice.php?pwd&whichchoice=741&option=3"); //master suite
	visit_url("choice.php?pwd&whichchoice=744&option=1"); //banish werewolves
return;
}

void vampires()
{
//spooky cabin vampires
	print("Checking out the lock on the Cabin first", "Green");

			string log = visit_url( "clan_raidlogs.php" );
		{
			if(log.contains_text( ") unlocked the attic of the cabin" ))
			{	print( "The attic of the cabin has been unlocked.", "green" );	}
			else
			{	print("The attic of the cabin has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}

	print("Checking out the lock on the ballroom next", "Green");
		{
			if(log.contains_text( ") unlocked the ballroom" ))
			{	print( "The ballroom has been unlocked, initiating banish", "green" );	}
			else
			{	print("The ballroom has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}
		{	string [int] drove;
	foreach place in $strings[some vampires out of the castle] 
	 {
			drove = split_string(log, place);
			if(count(drove) == 3)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " times.  No banishing needed in this zone", "green");
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash");
			return;	}
			else if(count(drove) == 2)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " time.  Continuing with banish");	}
			else { print("No one drove " + place); }
	}}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1");
	visit_url("choice.php?pwd&whichchoice=721&option=3"); //attic
	visit_url("choice.php?pwd&whichchoice=724&option=3"); //banish vampires
//hall
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=7");
	visit_url("choice.php?pwd&whichchoice=745&option=1"); //ballroom
	visit_url("choice.php?pwd&whichchoice=746&option=1"); //banish vampires
return;
}

void ghosts()
{
	print("Checking out the lock on the Fire Tree first", "Green");

			string log = visit_url( "clan_raidlogs.php" );
		{
			if(log.contains_text( ") unlocked the fire watchtower" ))
			{	print( "The fire watchtower has been unlocked.", "green" );	}
			else
			{	print("The fire watchtower has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}

	print("Checking out the lock on the Schoolhouse next", "Green");

		{
			if(log.contains_text( ") unlocked the schoolhouse" ))
			{	print( "The schoolhouse has also been unlocked, initiating banish", "green" );	}
			else
			{	print("The schoolhouse has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}
		{	string [int] drove;
	foreach place in $strings[some ghosts out of the village] 
	 {
			drove = split_string(log, place);
			if(count(drove) == 3)
			{	print("Someone already drove " + place+ " " + (count(drove) - 1) + " times.  No banishing needed in this zone", "green");
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash");
			return; }
			else if(count(drove) == 2)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " times.  Continuing with banish");	}
			else { print("No one drove " + place); }
	}}
//fire tree ghosts
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=2");
	visit_url("choice.php?pwd&whichchoice=725&option=2"); //fire tower
	visit_url("choice.php?pwd&whichchoice=732&option=1"); //banish ghosts
//village square
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=4");
	visit_url("choice.php?pwd&whichchoice=733&option=3"); //fright school
	visit_url("choice.php?pwd&whichchoice=734&option=1"); //banish ghosts
return;
}

void bugbears()
{ 
	print("Checking out the lock on the Fire Tower first", "Green");

			string log = visit_url( "clan_raidlogs.php" );
		{
			if(log.contains_text( ") unlocked the fire watchtower" ))
			{	print("The fire watchtower has been unlocked.", "green" );	}
			else
			{	print("The fire watchtower has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}

	print("Checking out the lock on the labratory next", "Green");
		{
			if(log.contains_text( ") unlocked the lab" ))
			{	print("The labratory has also been unlocked, initiating banish", "green" );	}
			else
			{	print("The labratory has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}
		}

		{	string [int] drove;
	foreach place in $strings[bugbears out of the forest] 
	 {
			drove = split_string(log, place);
			if(count(drove) == 3)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " times already.  No banishing needed in this zone", "green"); 
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash"); 
			return;}
			else if(count(drove) == 2)
			{	print("Someone drove " + place+ " " + (count(drove) - 1) + " time.  Continuing with banish");	}
			else { print("No one banished " + place); }
	}}
//below the roots
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=3");
	visit_url("choice.php?pwd&whichchoice=729&option=3"); //towards smelly
	visit_url("choice.php?pwd&whichchoice=732&option=1"); //banish bugbears
//tower
		
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=8");
	visit_url("choice.php?pwd&whichchoice=749&option=1"); //labratory
	visit_url("choice.php?pwd&whichchoice=750&option=1"); //banish bugbears
return;
}

void skeletons()
{

			string log = visit_url( "clan_raidlogs.php" );
		{	string [int] drove;
	foreach place in $strings[some skeletons out of the castle] 
	 {
			drove = split_string(log, place);
			if(count(drove) == 3)
			{	print("Someone drove skeletons out of the " + place+ " " + (count(drove) - 1) + " times.  No banishing needed in this zone", "green");	
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash");
			return;	}
			else if(count(drove) == 2)
			{	print("Some one drove skeletons out of the " + place+ " " + (count(drove) - 1) + " time.  Continuing with banish");	}
			else { print("No one banished skeletons out of the " + place); }
	}}
//skid row
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=5");
	visit_url("choice.php?pwd&whichchoice=737&option=3"); //8-9-10
	visit_url("choice.php?pwd&whichchoice=740&option=1"); //banish skeletons
//tower
	{
	if(my_class() != $class[sauceror] || (my_class() != $class[pastamancer]))
	{	print("You're not a Mysticality class, you can not skeletons from the tower.", "red");	exit;	}
	if(my_class() == $class[sauceror] || (my_class() == $class[pastamancer]))
	{	print("You are a Mysticality class, continuing to the Tower, to banish more skeletons.", "green");	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=8");
	visit_url("choice.php?pwd&whichchoice=749&option=2"); //labratory
	visit_url("choice.php?pwd&whichchoice=750&option=1"); //banish skeletons (myst only)	
	}
exit;
}

void zombies()
{

	print("Checking out the lock on the Tower first", "Green");
			string log = visit_url( "clan_raidlogs.php" );
		{
			if(log.contains_text( ") unlocked the tower" ))
			{	print("The tower has been unlocked, initiating banish", "green" );	}
			else
			{	print("The tower has not been unlocked yet.  Unlock, and re-run script.", "red"); exit;	}
		}
//duke's estate
		{	string [int] drove;
	foreach place in $strings[some zombies out of the village] 
	 {			if(count(drove) == 3)
			{	print("Someone already drove " + place+ " " + (count(drove) - 1) + " times.  No banishing needed in this zone", "green");
			cli_execute("Pause 3");
			cli_execute("call unlock dread.ash");
			return;	}
			else if(count(drove) == 2)
			{	print("Someone already drove " + place+ " " + (count(drove) - 1) + " time.  Continuing with banish");	}
			else { print("No one drove " + place); }
	}}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=6");
	visit_url("choice.php?pwd&whichchoice=741&option=1"); //family plot
	visit_url("choice.php?pwd&whichchoice=742&option=1"); //banish zmobies
//tower

	visit_url("clan_dreadsylvania.php?action=forceloc&loc=8");
	visit_url("choice.php?pwd&whichchoice=749&option=1"); //labratory
	visit_url("choice.php?pwd&whichchoice=750&option=2"); //banish zombies
exit;

}

void cabin()
	{
	print("Checking out the lock on the Cabin first", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the attic of the cabin" ))
			{	print( "The attic of the cabin has been unlocked, moving on", "green" );	return;}
			else print( "The attic of the cabin needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Attic will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1");
	visit_url("choice.php?pwd&whichchoice=721&option=3&choiceform3=Try+the+attic");
	visit_url("choice.php?pwd&whichchoice=724&option=6&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=721&option=6&choiceform6=Leave");
	return;
	}

void tree()
	{
	print(" ");
	print("Checking out the Fire Tree", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the fire watchtower" ))
			{	print( "The watchtower has been unlocked, moving on", "green" );	return;		}
			else print("The watchtower needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Fire Tower will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}

	visit_url("clan_dreadsylvania.php?action=forceloc&loc=2");
	visit_url("choice.php?pwd&whichchoice=725&option=2&choiceform2=Check+out+the+fire+tower");
	visit_url("choice.php?pwd&whichchoice=727&option=6&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=725&option=6&choiceform6=Leave");
	return;
	}

void Village()
	{
	print(" ");
	print("Checking out the Village Square", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the schoolhouse" ))
			{	print( "The schoolhouse has been unlocked, moving on", "green" );	return;		}
			else print("The schoolhouse needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Schoolhouse will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=4");
	visit_url("choice.php?pwd&whichchoice=733&option=1&choiceform1=The+schoolhouse");
	visit_url("choice.php?pwd&whichchoice=734&option=6&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=733&option=6&choiceform6=Leave");
	return;
	}

void Estate()
	{
	print(" ");
	print("Checking out the Old Dukes Estate", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the master suite" ))
			{	print( "The master suite has been unlocked, moving on", "green" );	return;		}
			else print("The master suite needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Master Suite will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=6");
	visit_url("choice.php?pwd&whichchoice=741&option=3&choiceform3=Make+your+way+to+the+master+suite");
	visit_url("choice.php?pwd&whichchoice=744&option=6&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=741&option=6&choiceform6=Leave");
	return;
	}

void GreatHall()
	{
	print(" ");
	print("Checking out the Great Hall", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the ballroom" ))
			{	print( "The ballroom has been unlocked, moving on", "green" );	return;		}
			else print("The ballroom needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Ballroom will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=7");
	visit_url("choice.php?pwd&whichchoice=745&option=1&choiceform1=Head+to+the+ballroom");
	visit_url("choice.php?pwd&whichchoice=746&option=6&choiceform6=Leave&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=745&option=6&choiceform6=Leave&choiceform6=Leave");

	print(" ");
	print("All done", "green");
	return;
	}
void Tower()
	{
	print(" ");
	print("Checking out the Tower", "Green");
		{
			string log = visit_url( "clan_raidlogs.php" );
			if(log.contains_text( ") unlocked the lab" ))
			{	print( "The lab has been unlocked.  All finished!", "green" );	return;		}
			else print("The lab needs to be unlocked.  Unlocking now.");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
		{
		if($item[Freddy Kruegerand].available_amount() >= 25 )
		{
		print ("Buying a key", "Green");
		visit_url("shop.php?pwd&whichshop=dv&action=buyitem&whichrow=147&bigform=Buy+Item&quantity=1");
		visit_url("clan_dreadsylvania.php");
		}
		else if($item[Freddy Kruegerand].available_amount() <= 24 )
		print_html ("<font color=#cc0000>You don't have any enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy a <font color=#3D0052>\"Dreadsylvanian Skeleton Key\".  Aborting");
		}
	if($item[Dreadsylvanian skeleton key].available_amount() <= 0 )
	{		print_html ("<font color=#cc0000>You don't have any <font color=#3D0052>\"Dreadsylvanian Skeleton Keys\"</font>, or enough <font color=#3D0052>\"Freddy Kruegerand\"</font> to buy them. The Lab will not be unlocked.");
			print_html ("<font color=#cc0000>Pease get a <font color=#3D0052>\"Dreasylvanian Skeleton Key\"</font> or some <font color=#3D0052>\"Freddy Kruegerand\"</font>, and try again.");	
		exit;	}
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=8");
	visit_url("choice.php?pwd&whichchoice=749&option=1&choiceform1=Go+to+the+laboratory");
	visit_url("choice.php?pwd&whichchoice=750&option=6&choiceform6=Leave&choiceform6=Leave");
	visit_url("choice.php?pwd&whichchoice=749&option=6&choiceform6=Leave&choiceform6=Leave");
	return;
	}

void yestower()
	{
	print ("You have chosen to unlock the tower.", "green");
		cabin();
		tree();
		Village();
		Estate();
		GreatHall();
		Tower();
	}

void cabinonly()
	{
	print ("You have chosen to unlock the Cabin only.", "green");
		cabin();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}

void treeonly()
	{
	print ("You have chosen to unlock the Tallest Tree only.", "green");
		tree();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}
void villageonly()
	{
	print ("You have chosen to unlock the Village only.", "green");
		Village();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}
void estateonly()
	{
	print ("You have chosen to unlock the Estate only.", "green");
		estate();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}
void greathallonly()
	{
	print ("You have chosen to unlock the Great Hall only.", "green");
		greathall();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}
void toweronly()
	{
	print ("You have chosen to unlock the Tower only.", "green");
		tower();
	cli_execute("pause 5");
	cli_execute("call unlock dread.ash");
	}

void kisses()
{
print("About Kisses:");
print("Each dungeon starts with a minimum of 3300 kisses.");
print("1000 kisses for each zone (Forest, Village, Castle), and 100 for each boss killed.");
print("Kisses are earned by defeating Monsters.");
print("Kisses are lost by BEING defeated by monsters in Dreadsylvania. (1 kiss per loss, regardless of diffiulty)");
print_html("Every time an element \(<font color=#FF0000>Hot,</font> <font color=#0000FF>Cold,</font> <font color=#008000>Stench,</font> <font color=#808080>Spooky,</font>, <font color=#800080>Sleaze</font>) is banished from a zone, the difficulty goes up, and so do the kisses.");
print("For example:");
print_html("If you banish <font color=#FF0000>Hot</font> monsters from the forest, you will now receive 2 kisses per win in the forest.  The forest is now eligible for 2000 kisses.");
print_html("If you banish <font color=#FF0000>Hot,</font> <font color=#0000FF>Cold,</font> <font color=#008000>Stench,</font> <font color=#800080>Sleaze</font> from the forest, you will now receive 5 kisses per win in the forest.  The forest is now eligible for 5000 kisses.");
print_html("If you banish ALL elements \(<font color=#FF0000>Hot,</font> <font color=#0000FF>Cold,</font> <font color=#008000>Stench,</font> <font color=#808080>Spooky,</font>, <font color=#800080>Sleaze</font>\) They will all return, and monsters will be extremly difficult, but that zone is now eligible for 6000 kisses.");
print("");
print("The number of kisses earned across the whole dungeon instance is theorised to affect how many consumables the boss drops.");
print("");
print("If the boss kill raised the total number of kisses to at least 3700 (not counting losses and runaways), a consumable item is dropped.");
print_html("<font color=#ff0000>NOTE:</font> The consumable is only dropped if kisses are 3700 or higher");
print("A second consumable item is dropped if the post-boss gross kiss total is at least 6900.");
print("A third consumable item is dropped if the post-boss gross kiss total is at least 11,000.");
print("A Hard Mode boss will additionally drop one of 3 bizarre, unique, powerful non-outfit items, plus one extra guaranteed consumable item.");
print("If all 3000 enemies were killed and the post-boss gross kiss total is at least 18,000, a Hard Mode boss will drop a second non-outfit item.  ");
print("(Defeating all three Hard Mode bosses in this fashion, with max Danger from the start, will earn three double-drops and the Platinum Smooch clan trophy.)");
print_html("<font color=#ff0000>NOTE:</font> The more Kisses in the dungeon, the harder the monsters are to kill!");
return;}

void drops()
{
print("About Monster Drops");
print_html("</font>If you banish a perticular element, then you will no longer fight that monsters element, in the zone you removed it from (Forest, Village, Castle)");
print("If banish an element from the Forest, you will still have that element in the Village, and Castle.");
print_html("You can only banish one element from one zone at a time.  If you want all <font color=#FF0000>Hot</font> Monsters removed from dungeon, you must banish from all 3 zones");
print("If you banish a Monster (max 2 times), then you (most likely) will no longer fight that monster type.");
print("\"The Powers That Be\" made the dungeon, so sometimes you will still encounter a monster that has been banished, although likely infrequently");
print(" ");
print_html("Here is a list of all the <font color=#00b33c>Monsters</font> you can encounter in Dreadsylvania and the Element associated with each.");
print_html("+-----------+------------------------+------------------------+-------------------------------------+--------------------------+-----------------------+");
print_html("|<font color=#00b33c>Monster</font>..|<font color=#FF0000>Hot</font>..........................|<font color=#0000FF>Cold</font>.........................|<font color=#008000>Stench</font>......................................|<font color=#808080>Spooky</font>......................|<font color=#800080>Sleaze</font>....................|");
print_html("+-----------+------------------------+------------------------+-------------------------------------+--------------------------+-----------------------+");
print_html("|<font color=#00b33c>Bugbear</font>.|<font color=#FF0000>warm fur</font>.................|<font color=#0000FF>snowstick</font>...............|<font color=#008000>stinkwater</font>................................|<font color=#808080>eerie fetish</font>................|<font color=#800080>dubious loincloth</font>.|");
print_html("|<font color=#00b33c>Werewolf</font>|<font color=#FF0000>accidental mutton</font>|<font color=#0000FF>drafty drawers</font>........|<font color=#008000>guts necklace</font>.........................|<font color=#808080>wolfskull mask</font>.........|<font color=#800080>groping claw</font>.........|");
print_html("|<font color=#00b33c>Zombie</font>...|<font color=#FF0000>hothammer</font>...........|<font color=#0000FF>Thriller Ice</font>...............|<font color=#008000>muddy skirt</font>.............................|<font color=#808080>grandfather watch</font>....|<font color=#800080>antique spyglass</font>.|");
print_html("|<font color=#00b33c>Ghost</font>......|<font color=#FF0000>vengeful spirit</font>.......|<font color=#0000FF>BOOtonniere</font>..........|<font color=#008000>bag of unfinished business</font>|<font color=#808080>ghost thread</font>..............|<font color=#800080>transparent pants</font>|");
print_html("|<font color=#00b33c>Skeleton</font>.|<font color=#FF0000>frying brainpan</font>......|<font color=#0000FF>old ball and chain</font>.|<font color=#008000>tailbone shield</font>.......................|<font color=#808080>old dry bone</font>..............|<font color=#800080>tonguebone</font>...........|");
print_html("|<font color=#00b33c>Vampire</font>..|<font color=#FF0000>vial of hot blood</font>....|<font color=#0000FF>remorseless knife</font>|<font color=#008000>cod cape</font>..................................|<font color=#808080>intimidating coiffure</font>|<font color=#800080>blood sausage</font>.....|");
print_html("+------------+-----------------------+------------------------+-------------------------------------+--------------------------+-----------------------+ ");
print(" ");
print("Examples:");
print_html("If you banish <font color=#FF0000>Hot</font>, you will not be able to farm \"<font color=#FF0000>warm fur</font>\" ");
print_html("If you banish <font color=#808080>Spooky</font>, you will not be able to farm for <font color=#808080>old dry bone</font>, and therefore not fight <font color=#0000cc>The Unkillable Skeleton</font> in hard mode, without buying parts");
return;}

void bosslist()
{
print_html ("+-----------+-----------------------------------------------+--------------------------+---------------------------------------------------+");
print_html ("|<font color=#00b33c>Location</font>:|<font color=#0000cc>Boss</font>:.....................................................|<font color=#660066>Setup Requirement</font>:|<font color=#ff0000>Hard Mode Requirement</font>:.......................|");
print_html ("+-----------+-----------------------------------------------+--------------------------+---------------------------------------------------+");
print_html ("|<font color=#00b33c>Forest</font>.....|<font color=#0000cc>Great Wolf of the Air</font>............................|<font color=#660066>Banish Bugbears</font>....|<font color=#ff0000>Wear a moon-amber necklace</font>..............|");
print_html ("|<font color=#00b33c>Forest</font>.....|<font color=#0000cc>Falls-From-Sky</font>....................................|<font color=#660066>Banish Werewolves</font>|<font color=#ff0000>Have the First Blood Kiwi effect</font>.............|");
print_html ("+-----------+-----------------------------------------------+--------------------------+---------------------------------------------------+");
print_html ("|<font color=#00b33c>Village</font>.....|<font color=#0000cc>Mayor Ghost</font> .......................................|<font color=#660066>Banish Zombies</font>......|<font color=#ff0000>Wear a Dreadsylvania Auditor's badge</font>|");
print_html ("|<font color=#00b33c>Village</font>.....|<font color=#0000cc>Zombie Homeowners' Association</font>|<font color=#660066>Banish Ghosts</font>.........|<font color=#ff0000>Wear a weedy skirt into the fight</font> ...........|");
print_html ("+-----------+-----------------------------------------------+--------------------------+---------------------------------------------------+");
print_html ("|<font color=#00b33c>Castle</font>.....|<font color=#0000cc>Count Drunkula</font>...................................|<font color=#660066>Banish Skeleton</font>......|<font color=#ff0000>Wear the ghost shawl into combat</font>.......|");
print_html ("|<font color=#00b33c>Castle</font>.....|<font color=#0000cc>The Unkillable Skeleton</font>....................|<font color=#660066>Banish Vampires</font>.....|<font color=#ff0000>Have the Shepherd's Breath effect</font> ......|");
print_html ("+-----------+-----------------------------------------------+--------------------------+---------------------------------------------------+");
return;}

void wolf()
{
print_html("For <font color=#00b90c>moon-amber</font>, if chosen to buy: ");
cli_execute("mall_price moon-amber");
print_html("For <font color=#00b90c>polished moon-amber</font>, if chosen to buy: ");
cli_execute("mall_price polished moon-amber");
print_html("Here are the the steps needed to obtain a <font color=#00b33c><b>Moon-Amber necklace</font>, which is needed to fight boss in Hard Mode");
print("As a Muscle class, ");
print("Step 1\) climb The Tallest Tree in the Forest, ");
print(".........1\) climb to top, and ");
print_html(".........2\) grab the shiny thing. - get <font color=#00b90c>moon-amber");
print_html("Muscle Class gives <font color=#00b90c>moon-amber</font> to Moxie Class for step 2");
print("As a Moxie class:");
print("Step 2\) Village - The Even More Dreadful Part of Town");
print(".........1\) Investigate the ticking shack");
print_html(".........2\) polish the <font color=#00b90c>moon-amber</font>. This gets the <font color=#00b90c>polished moon-amber");
print_html("Moxie Class gives <font color=#00b90c>polished moon amber</font> to Myst Class for step 3");
print_html("If a player does NOT have \"<font color=#00b33c><b>Enchanted Jewelry, Then and Now</font /b>\", then as a Mysticality class,");
print_html("Step 3\) Tower Most Tall");
print_html(".........1\) enter the library");
print_html(".........2\) read \"<font color=#00b33c><b>Enchanted Jewelry, Then and Now</font></b>\" to learn the jewelery-crafting recipe. \(One time adventure\)");
print_html("<font color=#ff0000>NOTE:</font> If a player has skill <font color=#00b33c><b>\"Enchanted Jewelry, Then and Now\"</font /b>, then they can craft the <font color=#00b33c><b>Moon-Amber necklace");
print_html("Step 4\) Craft <font color=#00b33c><b>Moon-Amber necklace</font></b>, with <font color=#00b90c>Heavy Necklace Chain</font>");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 5\) Equip <font color=#00b33c><b>Moon-Amber necklace</font></b> and kill <font color=#0000cc>Great Wolf of the Air");
return;}

void falls()
{
print_html("For <font color=#00b90c>blood kiwi</font>, if chosen to buy: ");
cli_execute("mall_price blood kiwi");
print_html("For <font color=#00b90c>Eau de mort</font>, if chosen to buy: ");
cli_execute("mall_price Eau de mort");
print_html("Here are the steps needed to obtain a <font color=#00b33c><b>Bloody Kiwitini</font>, which is needed to fight boss in Hard Mode");
print("2 Players must coordinate together to get blood kiwi.  1 must be a muscle class.");
print("As a Muscle class:");
print("Step 1A\) climb The Tallest Tree in the Forest, ");
print_html(".........1\) climb to top - <font color=#ff0000>Wait for clan member to be ready for fruit!");
print("..................\(With a clan member at the base of the tree and looking upward\)");
print(".........2\) Stomp on the fruit branch");
print("..................\(The clan member at the base of the tree gains a blood kiwi.\)");
print("As any other class:");
print("Step 1B\) The Tallest Tree in the Forest, ");
print(".........1\) Stand near the base looking upward");
print_html(".........2\) Keep staring upwards... -<font color=#ff0000> Until the blood kiwi drops.");
print("Step 2\) Old Dukes Estate");
print(".........1\) Make your way to Master Suite");
print_html(".........2\) Check out the nightstand - get <font color=#00b90c>Eau de mort");
print_html("Step 3\) Tower (As Moxie class only, with <font color=#00b90c>Eau de mort</font>, and <font color=#00b90c>blood kiwi</font>\)");
print(".........1\) Go to the Labratory");
print_html(".........2\) Use the still - get <font color=#00b33c><b>Bloody Kiwitini");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 4\) Drink <font color=#00b33c><b>Bloody Kiwitini</font></b>, and kill <font color=#0000cc>Falls-From-Sky</font>");
return;}

void mayor()
{
print_html("For <font color=#00b90c>wax banana</font>, if chosen to buy: ");
cli_execute("mall_price wax banana");
print_html("For <font color=#00b90c>complicated lock impression</font>, if chosen to buy: ");
cli_execute("mall_price complicated lock impression");
print_html("For <font color=#00b90c>replica key</font>, if chosen to buy: ");
cli_execute("mall_price replica key");
print_html("For <font color=#00b90c>intricate music box parts</font>, if chosen to buy: ");
cli_execute("mall_price intricate music box parts");
print_html("Here are the steps needed to obtain a <font color=#00b33c><b>Dreadsylvania Auditor's badge</font>, which is needed to fight boss in Hard Mode");
print("As a Mysticality class");
print("Step 1\) Great Hall");
print(".........1\) Investigate the Dining room");
print_html(".........2\) levitate in the rafters - get the <font color=#00b90c>wax banana");
print("Step 2\) Cabin");
print(".........1\) Go down to the basement");
print_html(".........2\) Stick a wax banana in the lock - get <font color=#00b90c>complicated lock impression");
print_html(".........3\) Give <font color=#00b90c>complicated lock impression</font> to Moxie class player");
print_html("Step 3\) Cabin, as an Accordion Thief, who did NOT make <font color=#00b90c>complicated lock impression");
print(".........1\) Try the attic");
print_html(".........2\) Turn off the music box - get <font color=#00b90c>intricate music box parts");
print_html("<font color=#ff0000>NOTE:</font> This will also banish spooky monsters from the Woods");
print_html("<font color=#ff0000>NOTE:</font> The more elements you banish, the harder the area will be, but better for getting what you need.");
print("Step 4\) As Moxie Class");
print(".........1\) Village");
print(".........2\) The Even More Dreadful Part of Town");
print(".........3\) Investigate the ticking shack");
print_html(".........4\) Make a key using the <font color=#00b90c>complicated lock impression</font> - get <font color=#00b90c>Replica Key");
print_html("Anyone with the <font color=#00b90c>replica key</font> in their inventory (who didn�t do either step 2 or 3 this run) can obtain the <font color=#00b33c><b>Dreadsylvania Auditor's badge</b></font> from the Forest.");
print_html("Step 5\) Cabin with <font color=#00b90c>replica key</font> and <font color=#00b90c>intricate music box parts</font> in your inventory");
print(".........1\) Basement");
print_html(".........2\) unlock the lockbox - get <font color=#00b33c><b>Dreadsylvania Auditor's badge");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 6\) Equip <font color=#00b33c><b>Dreadsylvania Auditor's badge</b></font> and kill <font color=#0000cc>Mayor Ghost</font>");
return;}

void skeleton()
{
print_html("For <font color=#00b90c>old dry bone</font>, if chosen to buy: ");
cli_execute("mall_price old dry bone");
print_html("For old <font color=#00b90c>stinking agaricus</font>, if chosen to buy: ");
cli_execute("mall_price stinking agaricus");
print_html("Here are the steps needed to obtain a <font color=#00b33c><b>Dreadsylvanian Shepards Pie</font>, which is needed to fight boss in Hard Mode");
print_html("<font color=#ff0000><b>NOTE:</font></b> Spooky Skeletons are the only one's to drop the <font color=#00b90c>old dry bone</font>.  Which is needed for bone flour");
print("Step 1A\) Banish 1 or more elements (Hot, Cold, Sleaze, Stench) out of forest to make Spooky Skeletons");
print(".........1\) Run Script again for banish information/automation");
print_html("<font color=#ff0000>NOTE:</font> The more elements you banish, the harder the area will be, but better for getting what you need.");
print("Step 1B\) Banish werewolves out of forest to make Stench Zombies easier to find");
print("Step 2\) Cabin ");
print(".........1\) kitchen");
print_html(".........2\) check out the spice rack - get the <font color=#00b90c>dread tarragon");
print_html("<font color=#ff0000>NOTE:</font> To get the <font color=#00b90c>bone flour</font>, you first need an <font color=#00b90c>old dry bone</font>, which is a drop from the spooky skeletons in the Castle");
print("As a Muscle class, With an <font color=#00b90c>old dry bone</font> in inventory");
print("Step 3\) Cabin");
print(".........1\) kitchen");
print_html(".........2\) screw around with the flour mill - get <font color=#00b90c>bone flour");
print("Step 4\) Great Hall");
print(".........1\) dining room");
print(".........2\) grab the roast.");
print("Step 5\) Dungeons");
print(".........1\) check out the guardroom");
print_html(".........2\) break off some choice bits - get <font color=#00b90c>stinking agaricus");
print_html("<font color=#ff0000>NOTE:</font>Pastamancer or Sauceror needed to cook <font color=#00b33c><b>Dreadsylvanian Shepards Pie");
print("Myst Class");
print("Step 6\) The Old Duke's Estate");
print(".........1\) head into the servant's quarters");
print_html(".........2\) bake the pie - get <font color=#00b33c><b>Dreadsylvanian Shepards Pie");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 7\) Eat <font color=#00b33c><b>Dreadsylvanian Shepards Pie</font></b>, and kill <font color=#0000cc>The Unkillable Skeleton");
return;}

void ZHA()
{
print_html("For <font color=#00b90c>muddy skirt</font>, if chosen to buy: ");
cli_execute("mall_price muddy skirt");
print_html("Here are the steps needed to obtain a <font color=#00b33c><b>Weedy Skirt</font>, which is needed to fight boss in Hard Mode");
print_html("<font color=#ff0000><b>NOTE:</font></b> Stench Zombies are the only one's to drop the <font color=#00b90c>Muddy Skirt");
print("Banish werewolves to Guarantee <font color=#0000cc>Zombie Homeowners' Association</font> will show up to fight.");
print("Step 1A\) Cabin");
print(".........1\) Attic");
print(".........2\) Check out the wolfsbane");
print("Step 1B\) Dukes Estate");
print(".........1\) Master Suite");
print(".........2\) Blow the whistle");
print("Step 2\) Banish 1 or more elements (Hot, Cold, Sleaze, Spooky) out of forest to make Stench Zombies");
print(".........1\) Run Script again for banish information/automation");
print_html("<font color=#ff0000>NOTE:</font> The more elements you banish, the harder the area will be, but better for getting what you need.");
print_html("<font color=#ff0000>NOTE:</font> Only do Step 3 if you need the <font color=#00b90c>Dreadsyvanian seed pod");
print("Step 3\) Tallest Tree");
print(".........1\) Root around at the base");
print_html(".........2\) Stand near the base looking downward - Get <font color=#00b90c>Dreadsyvanian seed pod");
print("Step 4\) Hall");
print(".........1\) Ballroom");
print_html(".........2\) \"Trip the light fantastic\" with a <font color=#00b90c>muddy skirt</font> equipped and a <font color=#00b90c>Dreadsyvanian seed pod</font> in your inventory.");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 5\) Equip <font color=#00b33c><b>Weedy Skirt</font></b> and kill <font color=#0000cc>Zombie Homeowners' Association");
return;}

void drunkula()
{
print_html("Here are the steps needed to obtain a <font color=#00b33c><b>Ghost Shawl");
print_html("Price for each <font color=#00b90c>ghost thread</font>, if chosen to buy: ");
cli_execute("mall_price ghost thread");
print_html("<font color=#ff0000><b>NOTE:</font></b> Spooky Ghosts are the only one's to drop the <font color=#00b90c>Ghost Threads");
print_html("Banish Zombies to guarantee <font color=#0000cc>Count Drunkula</font> will show up to fight");
print("Step 1A\) Dukes Estate");
print(".........1\) Family Plot");
print(".........2\) Close Gates");
print("Step 1B\) Tower");
print(".........1\) Labratory");
print(".........2\) Examine brains");
print("Step 2\) Banish 1 or more elements (Hot, Cold, Sleaze, Stench) out of Village to make Spooky Ghosts");
print(".........1\) Run Script again for banish information/automation");
print_html("<font color=#ff0000>NOTE:</font> The more elements you banish, the harder the area will be, but better for getting what you need.");
print("Step 3\) Banish Skeletons out of Village, to make Ghosts easier to find");
print_html("Step 4\) Fight Spooky Ghosts to get <font color=#ff0000>10 <font color=#00b90c</b>ghost threads");
print_html("<font color=#ff0000><b>NOTE:</font></b> Before you continue to next step, you must have <font color=#ff0000>10 <font color=#00b90c>Ghost Threads");
print("Step 5\) Duke's Estate");
print(".........1\) Make your way to the Master Suite");
print_html(".........2\) Mess with the loom - Get <font color=#00b33c><b>Ghost Shawl");
print_html("<font color=#ff0000>NOTE:</font> The next step must be done by the boss killer.");
print_html("Step 6\) Equip <font color=#00b33c><b>Ghost Shawl</font></b> and kill <font color=#0000cc>Count Drunkula");
return;}

void forestsleaze()
    {
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the forest less sleazy" ))
		{	print( "The castle has been made less sleazy already moving on");
		return; }
	if((my_class() != $class[seal clubber]) || (my_class() != $class[turtle tamer]))
         {
          print("You need to be a muscle class to banish sleaze from the forest", "red");
          return;
         }
	else {
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=2"); //tallest tree
	visit_url("choice.php?pwd&whichchoice=725&option=1"); //climb to top, muscle only
	visit_url("choice.php?pwd&whichchoice=726&option=2"); //kick the nest
	return;
	}}

void foreststinky()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the forest less stinky" ))
		{
		print( "The castle has been made less stinky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1"); //cabin
	visit_url("choice.php?pwd&whichchoice=721&option=1"); //kitchen
	visit_url("choice.php?pwd&whichchoice=722&option=3"); //disposal
	return;
	}

void forestspooky()
	{
	string log = visit_url( "clan_raidlogs.php" );
	if(log.contains_text( " unlocked the attic of the cabin" ))
	{	print("The attic of the cabin has been unlocked.  Continuing wish banish");	}
	else
	{	print("The attic of the cabin has not been unlocked yet.  Unlock, and re-run script.", "red"); return;	}

	if(log.contains_text( " made the forest less spooky" ))
		{	print( "The castle has been made less spooky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1"); //cabin
	visit_url("choice.php?pwd&whichchoice=721&option=3"); //attic
	visit_url("choice.php?pwd&whichchoice=724&option=1"); //turn off music box
	return;
	}

void foresthot()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the forest less hot" ))
		{	print( "The castle has been made less hot already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=3"); //burrows
	visit_url("choice.php?pwd&whichchoice=729&option=1"); //towards heat
	visit_url("choice.php?pwd&whichchoice=730&option=1"); //pull cork
	return;
	}

void forestcold()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the forest less cold" ))
		{	print( "The castle has been made less cold already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=1"); //cabin
	visit_url("choice.php?pwd&whichchoice=729&option=2"); //towards cold
	visit_url("choice.php?pwd&whichchoice=731&option=1"); //read the heart a story
	return;
	}

void villagehot()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the village less hot" ))
		{	print( "The village has been made less hot already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=6"); //Dukes Estate
	visit_url("choice.php?pwd&whichchoice=741&option=2"); //Servents quarters
	visit_url("choice.php?pwd&whichchoice=743&option=1"); //Turn off the ovens
	return;
	}

void villagecold()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the village less cold" ))
		{	print( "The village has been made less cold already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=4"); //Village Square
	visit_url("choice.php?pwd&whichchoice=733&option=2"); //Blacksmith
	visit_url("choice.php?pwd&whichchoice=735&option=1"); //Stoke the furnace
	return;
	}

void villagespooky()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the village less spooky" ))
		{	print( "The village has been made less spooky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=4"); //Village Square
	visit_url("choice.php?pwd&whichchoice=733&option=3"); //Gallows
	visit_url("choice.php?pwd&whichchoice=736&option=1"); //Paint the noose pink
	return;
	}

void villagesleazy()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the village less sleazy" ))
		{	print( "The village has been made less sleazy already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=5"); //Skid Row
	visit_url("choice.php?pwd&whichchoice=737&option=2"); //Eight, Nine, Tenement
	visit_url("choice.php?pwd&whichchoice=740&option=2"); //Paint over the graffiti
	return;
	}

void villagestinky()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the village less stinky" ))
		{	print( "The village has been made less stinky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=5"); //Skid Row
	visit_url("choice.php?pwd&whichchoice=737&option=1"); //Sewers
	visit_url("choice.php?pwd&whichchoice=738&option=1"); //Unclog the gate
	return;
	}

void castlehot()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the castle less hot" ))
		{	print( "The forest has been made less hot already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=9"); //Dungeons
	visit_url("choice.php?pwd&whichchoice=753&option=2"); //Boiler Room
	visit_url("choice.php?pwd&whichchoice=755&option=1"); //Let off some steam
	return;
	}

void castlecold()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the castle less cold" ))
		{	print( "The forest has been made less cold already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=7"); //Great Hall
	visit_url("choice.php?pwd&whichchoice=745&option=2"); //Kitchen
	visit_url("choice.php?pwd&whichchoice=747&option=1"); //Turn down the freezer
	return;
	}

void castlespooky()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the castle less spooky" ))
		{	print( "The forest has been made less spooky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=9"); //Dungeons
	visit_url("choice.php?pwd&whichchoice=753&option=1"); //Cell Block
	visit_url("choice.php?pwd&whichchoice=754&option=1"); //Flush the toilet
	return;
	}

void castlesleazy()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the castle less sleazy" ))
		{	print( "The forest has been made less sleazy already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=8"); //Tower
	visit_url("choice.php?pwd&whichchoice=749&option=3"); //Bedroom
	visit_url("choice.php?pwd&whichchoice=752&option=1"); //Shut the parrot up
	return;
	}

void castlestinky()
	{
	string log = visit_url( "clan_raidlogs.php" );
		if(log.contains_text( " made the castle less stinky" ))
		{	print( "The forest has been made less stinky already moving on");
		return; }
	visit_url("clan_dreadsylvania.php?action=forceloc&loc=7"); //Great Hall
	visit_url("choice.php?pwd&whichchoice=745&option=3"); //Dining room
	visit_url("choice.php?pwd&whichchoice=748&option=1"); //Clear the dishes
	return;
	}



void main(string Dreadsylvania)
{

	//Parse Desired_Banishes:
	effect requested_effect;
	Dreadsylvania = Dreadsylvania.to_lower_case();
	print_html("<font size=10><font color=#3D0052><b>D<font color=#45005C>r<font color=#4C0066>e<font color=#540070>a<font color=#5C007A>d<font color=#630085>s<font color=#6B008F>y<font color=#730099>l<font color=#7A00A3>v<font color=#8200AD>a<font color=#8A00B8>n<font color=#9100C2>i<font color=#9900CC>a");
	print_html("<font size=3> "); 

	if (dreadsylvania.contains_text("boss"))
	{	bosslist();	}
	else if (dreadsylvania.contains_text("wolf"))
	{	wolf();	}
	else if (dreadsylvania.contains_text("falls"))
	{	falls();	}
	else if (dreadsylvania.contains_text("mayor"))
	{	mayor();	}
	else if (dreadsylvania.contains_text("zha"))
	{	ZHA();	}
	else if (dreadsylvania.contains_text("kisses"))
	{	kisses();	}
	else if (dreadsylvania.contains_text("drunkula"))
	{	drunkula();	}
	else if (dreadsylvania.contains_text("skeleton"))
	{	skeleton();	}
	else if (dreadsylvania.contains_text("drops"))
	{	drops();	}
	else if (dreadsylvania.contains_text("all"))
	{	yestower();	}
	else if (dreadsylvania.contains_text("cabin"))
	{	cabinonly();	}
	else if (dreadsylvania.contains_text("tree"))
	{	treeonly();	}
	else if (dreadsylvania.contains_text("villagesleazy"))
	{	villagesleazy();	}
	else if (dreadsylvania.contains_text("villagestinky"))
	{	villagestinky();	}
	else if (dreadsylvania.contains_text("villagespooky"))
	{	villagespooky();	}
	else if (dreadsylvania.contains_text("villagecold"))
	{	villagecold();	}
	else if (dreadsylvania.contains_text("villagehot"))
	{	villagehot();	}
	else if (dreadsylvania.contains_text("village"))
	{	Villageonly();	}
	else if (dreadsylvania.contains_text("estate"))
	{	Estateonly();	}
	else if (dreadsylvania.contains_text("hall"))
	{	GreatHallonly();	}
	else if (dreadsylvania.contains_text("tower"))
	{	Toweronly();	}
	else if (dreadsylvania.contains_text("castlesleazy"))
	{	castlesleazy();	}
	else if (dreadsylvania.contains_text("castlestinky"))
	{	castlestinky();	}
	else if (dreadsylvania.contains_text("castlespooky"))
	{	castlespooky();	}
	else if (dreadsylvania.contains_text("castlecold"))
	{	castlecold();	}
	else if (dreadsylvania.contains_text("castlehot"))
	{	castlehot();	}
	else if (dreadsylvania.contains_text("forestsleazy"))
	{	forestsleaze();	}
	else if (dreadsylvania.contains_text("foreststinky"))
	{	foreststinky();	}
	else if (dreadsylvania.contains_text("forestspooky"))
	{	forestspooky();	}
	else if (dreadsylvania.contains_text("forestcold"))
	{	forestcold();	}
	else if (dreadsylvania.contains_text("foresthot"))
	{	foresthot();	}
	else if (dreadsylvania.contains_text("wolves"))
	{	wolves();	}
	else if (dreadsylvania.contains_text("skeletons")) 
	{	skeletons();	}
	else if (dreadsylvania.contains_text("zombies"))
	{	zombies();	}
	else if (dreadsylvania.contains_text("bugbears"))
	{	bugbears();	}
	else if (dreadsylvania.contains_text("vampires"))
	{	vampires();	}
	else if (dreadsylvania.contains_text("ghosts"))
	{	ghosts();	}
	else if (dreadsylvania.contains_text("help") || (Dreadsylvania.contains_text("")))
		{
		print_html ("<font color=008000>Here's the commands I understand, anything in <font color=7900a7><b>BOLD:"); 
		print_html ("To unlock the Cabin, Tallest Tree, Village, Old Dukes Estate, Great Hall, and Tower: <font color=7900a7><b>All");
		print_html ("Unlock Zones: <font color=7900a7><b>Cabin</b></font>, or Tallest <font color=7900a7><b>Tree</b></font>, or<font color=7900a7><b> Village</b></font>, or Old Dukes <font color=7900a7><b>Estate</b></font>, or Great <font color=7900a7><b>Hall</b></font>, or the<font color=7900a7><b> Tower</b></font> only");
		print_html ("Banish: <font color=7900a7><b>Wolves</b></font>,<font color=7900a7><b> Skeletons</b></font>,<font color=7900a7><b> Zombies</b></font>,<font color=7900a7><b> Bugbears</b></font>,<font color=7900a7><b> Vampires</b></font>, or<font color=7900a7><b> Ghosts</b></font>");
		print_html ("To banish an Element: ZoneElement");
		print_html ("Zones: <font color=7900a7><b>Village</font></b>, <font color=7900a7><b>Forest</font></b>, <font color=7900a7><b>Castle</font></b> Elements: <font color=7900a7><b>Hot</font></b>, <font color=7900a7><b>Cold</font></b>, <font color=7900a7><b>Spooky</font></b>, <font color=7900a7><b>Sleazy</font></b>, <font color=7900a7><b>Stinky");
		print_html ("Example Element banish: <font color=7900a7>Foresthot</font>, or <font color=7900a7>Castlestinky</font>, or <font color=7900a7>Villagespooky</font>, any combo works.");
		print_html ("<font color=#ff0000>NOTE:</font> The Script will continue to reload until you decide to cancel.");
		print(" ");
		print("Below this line is a basic Dreadsylvania help guide");
		print_html ("Basic info on <font color=#0000cc>Kisses</font>: <font color=7900a7><b>Kisses");
		print_html ("Basic info on <font color=#0000cc>Hard Mode Bosses: <font color=7900a7><b>Boss");
		print_html ("Best items Monsters Drop: <font color=7900a7><b>Drops");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Great Wolf of the Air</font>:<font color=7900a7><b> Wolf");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Falls-From-Sky</font>:<font color=7900a7><b> Falls");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Mayor Ghost</font>:<font color=7900a7><b> Mayor");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Zombie Homeowners' Association</font>:<font color=7900a7><b> ZHA");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>Count Drunkula</font>:<font color=7900a7><b> Drunkula");
		print_html ("Normal/Hard Mode setup for <font color=#0000cc>The Unkillable Skeleton<font color=7900a7><b> Skeleton");
		print_html ("To Exit this script, click Cancel button");
		}
}
